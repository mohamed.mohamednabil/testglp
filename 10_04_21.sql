SET DEFINE OFF;
Insert into TBI_KPI_SETUP
   (KPI_ID, SQLDESC, SQLSTAT, FORMULA, XML_PARAM, 
    CHANNEL_NAME, CATEGORY, LOGNAME, LOGDATE, FLEX_FLD1, 
    FLEX_FLD2, FLEX_FLD3, FLEX_FLD4, FLEX_FLD5)
 Values
   ('B2B_AUTH_INFO', 'B2B_AUTH_INFO', 'SELECT distinct a.rel_type,cn.NAME ap_name,ci.id_type,ci.id_no ap_idno,  
a.logdate, s.CONTACT_METHOD,s.CONTACT_INFO ,s.logdate contact_logdate, t.name contname, t.ar_name 
FROM  crm_relations a, crm_cust_names cn, crm_cust_info ci, CRM_CONTACT_DETAILS s , crm_Departement t
WHERE  cn.cust_no = ci.cust_no
   AND cn.cust_type = ci.cust_type
   AND ci.cust_no = a.crm_cust_no
   AND ci.cust_type = a.crm_cust_type
   and s.cust_no=a.crm_cust_no
   and s.cust_Type=a.crm_cust_Type
   and ((rel_type in (''KAM'',''SAM'',''AUTH'')  and ''&REL_TYPE'' is null ) or rel_type=''&REL_TYPE''  )
  -- and t.id_no=''1010181305''
  and t.contrno=''&CONTRNO''
   and t.id_type=''R''
   and CONTACT_METHOD is not null
    and    t.cust_no=a.cust_no
    and t.cust_type=a.cust_type', NULL, NULL, 
    'IVR', 'API', user, sysdate, NULL, 
    NULL, NULL, NULL, NULL);
Insert into TBI_KPI_SETUP
   (KPI_ID, SQLDESC, SQLSTAT, FORMULA, XML_PARAM, 
    CHANNEL_NAME, CATEGORY, LOGNAME, LOGDATE, FLEX_FLD1, 
    FLEX_FLD2, FLEX_FLD3, FLEX_FLD4, FLEX_FLD5)
 Values
   ('GET_LINES_PERCAT', 'GET_LINES_PERCAT', 'select * from  ( select subno , rownum  num   from  crm_user_info where contrno=''&CONTRNO'' and status <>''60'' and  subno like decode(''&CAT'',''FTTH'',''70%'',''VOICE'',''5%'',''DATA'',''831%'',''M2M'',''830%'') ) where num between ''&NUM_FROM'' and ''&NUM_TO'' and rownum <1000', NULL, NULL, 
    'IVR', 'API', user, sysdate, NULL, 
    NULL, NULL, NULL, NULL);
COMMIT;
SET DEFINE OFF;
Insert into TBI_KPI_SETUP
   (KPI_ID, SQLDESC, SQLSTAT, FORMULA, XML_PARAM, 
    CHANNEL_NAME, CATEGORY, LOGNAME, LOGDATE, FLEX_FLD1, 
    FLEX_FLD2, FLEX_FLD3, FLEX_FLD4, FLEX_FLD5)
 Values
   ('GET_AUTH_PER_CONTRACTS', 'GET_AUTH_PER_CONTRACTS', ' select distinct MEMP_CONTRNO from  
  crm_departement c, ipm_memb_info i, crm_user_info U  
  where c.contrno = i.MEMP_CONTRNO and  web_admin=''Y'' and c.ID_NO=''&ID_NO''   and c.id_type=''R''and ( U.ID_NO =''&AP_ID_NO'')   AND u.subno=i.MSISDN', NULL, NULL, 
    'IVR', 'API', user, sysdate, NULL, 
    NULL, NULL, NULL, NULL);
COMMIT;


SET DEFINE OFF;
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('B2B_AUTH_INFO', 'REL_TYPE', 2, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('B2B_AUTH_INFO', 'CONTRNO', 1, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_LINES_PERCAT', 'CAT', 2, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_LINES_PERCAT', 'NUM_FROM', 3, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_LINES_PERCAT', 'NUM_TO', 4, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_LINES_PERCAT', 'CONTRNO', 1, 'IN', 'VARCHAR2');





SET DEFINE OFF;
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_AUTH_PER_CONTRACTS', 'ID_NO', 1, 'IN', 'VARCHAR2');
Insert into TBI_KPI_SETUP_PARAMS
   (KPI_ID, PARAM_NAME, PARAM_SEQ, PARAM_INOUT, PARAM_TYPE)
 Values
   ('GET_AUTH_PER_CONTRACTS', 'AP_ID_NO', 2, 'IN', 'VARCHAR2');
COMMIT;
